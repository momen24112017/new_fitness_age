import { Component, OnInit} from '@angular/core';
import {ServicesService} from '../services.service';
import * as $ from 'jquery';
import 'bootstrap';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {

  public arrGroupExercisePrograms:any = []
  public arrFitnessPrograms:any = []
  public arrGroupExercise:any = [];
  public arrFitness:any = [];
  
  public arrTodayGroup:any =[];
  public arrTodayFitness:any = [];
  public arrRecentWeekGroup:any;
  public arrRecentWeekFitness:any = [];
  
  public arrVideoLibaryGroupExercise:any = [];
  public arrVideoLibaryGroupExerciseProgram:any = [];
  public arrVideoLibaryFitness:any = [];
  public arrVideoLibaryFitnessProgram:any = [];
  
  public arrHealthVideo:any = [];
  constructor(private _servicesService:ServicesService) { }

  ngOnInit() {
    
    this.listTodayClasses();
    this.listBlog();
    this.listProgram();
    this.listHealthVideo();
    this.listVideoLibary();
  }
  public getId = function(url) {
    url = url.trim();
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
      ? match[2]
      : null;
  }
  public listTodayClasses = function(){
    
    return this._servicesService.listTodayClasses().subscribe((data:Response)=>{
      var resultArray = Object.keys(data).map(function(classNamedIndex){
        let Data = data[classNamedIndex];
        
        return Data;
      });
      // this._servicesService.arrTodayGroup = resultArray;
      let arrTodayClass = resultArray;
      for(let value of arrTodayClass) {
        for(let value1 of value.data){
          if(value1.category == "group_exercise"){
            var videoId = this.getId(value1.link);
            value1.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
          
            var todayDate = new Date();
            var todayDateDay= todayDate.getDate();
            var dateTime1 = value1.date.split("T")
            var date = dateTime1[0].split("-");
            value1.date = date[2] +"-"+ date[1] +"-"+ date[0]
            if(value1.time > "12:00"){
                var t = value1.time.split(":")
                value1.time1 = parseInt(value1.time) - 12
                value1.time1 = value1.time1 + ":"+ t[1]+" PM"
            }else{
                value1.time1 = value1.time + " AM"
            }
            value1.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
            if(todayDateDay > parseInt(date[2])){

                this.arrRecentWeekGroup.push(value1);
                
            }else{
              
                this.arrTodayGroup.push(value1);
                
            }
            this._servicesService.arrRecentWeekGroup.next(this.arrRecentWeekGroup);
            this._servicesService.arrTodayGroup.next(this.arrTodayGroup);
            
          }else{
            var videoId = this.getId(value1.link);
            value1.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
            var dateTime1 = value1.date.split("T")
            var date = dateTime1[0].split("-");
            value1.date = date[2] +"-"+ date[1] +"-"+ date[0]
            if(value1.time > "12:00"){
                var t = value1.time.split(":")
                value1.time1 = parseInt(value1.time) - 12
                value1.time1 = value1.time1 + ":"+ t[1]+" PM"
            }else{
                value1.time1= value1.time + " AM"
            }
            value1.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0"
            if(todayDateDay > parseInt(date[2])){
                this.arrRecentWeekFitness.push(value1);
            }else{
                this.arrTodayFitness.push(value1);
            }
            this._servicesService.arrRecentWeekFitness.next(this.arrRecentWeekFitness);
            this._servicesService.arrTodayFitness.next(this.arrTodayFitness);
            // $scope.arrTodayFitness.push(value1);
          }
        }      
      }
      
    })
    
  }

  public listProgram = function(){
    return this._servicesService.listProgram().subscribe((data:Response)=>{
      var resultArray = Object.keys(data).map(function(classNamedIndex){
        let Data = data[classNamedIndex];
        
        return Data;
      });
      let arrProgram = resultArray;
      for(let value of arrProgram) {
        if(value.data[0].category == 'group_exercise'){
          for(let value1 of value.data) {
              
                  var videoId = this.getId(value1.link);
                  value1.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
              
                  var dateTime1 = value1.date.split("T")
                  var date = dateTime1[0].split("-");
                  value1.date = date[2] +"-"+ date[1] +"-"+ date[0]
                  if(value1.time > "12:00"){
                      var t = value1.time.split(":")
                      value1.time = parseInt(value1.time) - 12
                      value1.time = value1.time + ":"+ t[1]+" PM"
                  }else{
                      value1.time = value1.time + " AM"
                  }
                  value1.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
          
          }
          this.arrGroupExercisePrograms.push(value)

        
        }if(value.data[0].category == 'fitness'){
          for(let value1 of value.data){
              
              var videoId = this.getId(value1.link);
              value1.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
          
              var dateTime1 = value1.date.split("T")
              var date = dateTime1[0].split("-");
              value1.date = date[2] +"-"+ date[1] +"-"+ date[0]
              if(value.time > "12:00"){
                  var t = value1.time.split(":")
                  value1.time = parseInt(value1.time) - 12
                  value1.time = value1.time + ":"+ t[1]+" PM"
              }else{
                  value1.time = value1.time + " AM"
              }
              value1.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
            
          }
        this.arrFitnessPrograms.push(value)        
        }
      }
      this._servicesService.arrGroupExercisePrograms.next(this.arrGroupExercisePrograms);
      this._servicesService.arrFitnessPrograms.next(this.arrFitnessPrograms);
    }) 

    
  }

 
 
  public listHealthVideo = function(){
    return this._servicesService.listHealthVideo().subscribe((data:Response)=>{
      var resultArray = Object.keys(data).map(function(classNamedIndex){
        let Data = data[classNamedIndex];
        
        return Data;
      })
      let arrHealthVideo = resultArray
      for(let value of arrHealthVideo) {
               
        var videoId = this.getId(value.link);
        value.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
        value.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
        this.arrHealthVideo.push(value);
      }
      this._servicesService.arrHealthVideo.next(this.arrHealthVideo);
    })
  }
  public listBlog = function(){
    
    return this._servicesService.listBlogs().subscribe((data:Response)=>{
      this._servicesService.arrBlog.next(data);
    })
  }
  

  public listVideoLibary = function(){
    return this._servicesService.listVideoLibary().subscribe((data:Response)=>{
      var resultArray = Object.keys(data).map(function(classNamedIndex){
        let Data = data[classNamedIndex];
        
        return Data;
      })
      let arrProgramLibaryVideo = resultArray;
      for(let value of arrProgramLibaryVideo) {
          if(value.data[0].subCategory == 'group_exercise'){
              this.arrVideoLibaryGroupExerciseProgram.push(value)
              
          }else{
              this.arrVideoLibaryFitnessProgram.push(value)
              
          }
  
      }
      this._servicesService.arrVideoLibaryGroupExerciseProgram.next(this.arrVideoLibaryGroupExerciseProgram);
      this._servicesService.arrVideoLibaryFitnessProgram.next(this.arrVideoLibaryFitnessProgram); 
        
    })
  }
  
}
