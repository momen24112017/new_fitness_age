import { Component, OnInit } from '@angular/core';
import {ServicesService} from '../services.service';
import * as $ from 'jquery';
@Component({
  selector: 'app-libarytab',
  templateUrl: './libarytab.component.html',
  styleUrls: ['./libarytab.component.css']
})
export class LibarytabComponent implements OnInit {
  public arrVideoLibaryGroupExerciseProgram:any = [];
  public arrVideoLibaryFitnessProgram:any = [];
  public arrVideoLibaryFitness:any = [];
  public arrVideoLibaryGroupExercise:any = [];
  constructor(private _servicesService:ServicesService) { }

  ngOnInit() {
    $("#GprogramarrayNameback").hide();
    $("#fprogramarrayName").hide();
    this._servicesService.arrVideoLibaryGroupExerciseProgram.subscribe((data) => {
      // console.log(data);
      this.arrVideoLibaryGroupExerciseProgram = data;
      
    });
    this._servicesService.arrVideoLibaryFitnessProgram.subscribe((data) => {
      // console.log(data);
      this.arrVideoLibaryFitnessProgram = data;
      
    });
    this._servicesService.arrVideoLibaryGroupExercise.subscribe((data) => {
      // console.log(data);
      this.arrVideoLibaryGroupExercise = data;
      
    });
    this._servicesService.arrVideoLibaryFitness.subscribe((data) => {
      // console.log(data);
      this.arrVideoLibaryFitness = data;
      
    });
  }
  public getFLibaryVideos = function(obj){
    $('#fProgram').hide();
    $('#fprogramarray').show();
    $('#fprogramarrayName').show();
    this._servicesService.programNameF = obj.program.name
    this.arrVideoLibaryFitness = [];
    for(let value of this.arrVideoLibaryFitnessProgram) {
        if(value.program._id == obj.program._id){
            for(let value1 of value.data) {
                        
                var videoId = this.getId(value1.link);
                value1.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
            
                
                value1.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
                    
                this.arrVideoLibaryFitness.push(value1)
                
            }
        }
    }
    
  }

  public backtoProgramFitness= function(){
    $('#fprogramarray').hide();
    $('#fProgram').show();
    $('#fprogramarrayName').hide();

  }
  public getGLibaryVideos = function(obj){
    $('#GProgram').hide();
    $('#Gprogramarray').show();
    $('#GprogramarrayName').show();
    $('#GprogramarrayNameback').show();
    this._servicesService.programNameG = obj.program.name
    this.arrVideoLibaryGroupExercise = [];
    for(let value of this.arrVideoLibaryGroupExerciseProgram) {
        if(value.program._id == obj.program._id){
            for(let value1 of value.data) {
                var videoId = this.getId(value1.link);
                value1.image = 'http://i3.ytimg.com/vi/'+ videoId +'/hqdefault.jpg'
            
                
                value1.emb = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";

                this.arrVideoLibaryGroupExercise.push(value1)
            }
        }
  
    }
    
  }
  public backtoProgramGroup= function(){
      $('#Gprogramarray').hide();
      $('#GProgram').show();
      $('#GprogramarrayName').hide();
      $('#GprogramarrayNameback').hide();

  } 
  public getId = function(url) {
    url = url.trim();
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
      ? match[2]
      : null;
  }
  public video = function(obj){
    return this._servicesService.video(obj).subscribe((data:Response)=>{

    })
  }
}
