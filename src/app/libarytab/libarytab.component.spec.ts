import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibarytabComponent } from './libarytab.component';

describe('LibarytabComponent', () => {
  let component: LibarytabComponent;
  let fixture: ComponentFixture<LibarytabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibarytabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibarytabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
