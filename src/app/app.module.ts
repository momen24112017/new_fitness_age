import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ClassComponent } from './class/class.component';
import { StayComponent } from './stay/stay.component';
import { VideosComponent } from './videos/videos.component';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';



import {HttpClient ,HttpClientModule } from '@angular/common/http';







import { GroupprogComponent } from './groupprog/groupprog.component';
import { GrouptabComponent } from './grouptab/grouptab.component';
import { FitnesstabComponent } from './fitnesstab/fitnesstab.component';
import { LibarytabComponent } from './libarytab/libarytab.component';
import { HealthComponent } from './health/health.component';
import { BlogtabComponent } from './blogtab/blogtab.component';
import { IntroComponent } from './intro/intro.component';
import { LandbookComponent } from './landbook/landbook.component';
import { FooterComponent } from './footer/footer.component';
import { NationalityComponent } from './nationality/nationality.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClassComponent,
    StayComponent,
    VideosComponent,
    GroupprogComponent,
    GrouptabComponent,
    FitnesstabComponent,
    LibarytabComponent,
    HealthComponent,
    BlogtabComponent,
    IntroComponent,
    LandbookComponent,

    FooterComponent,
    NationalityComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    
    HttpClientModule,

    
  
    AppRoutingModule,
    BrowserAnimationsModule,
    
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule,

    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgbModule
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
