import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupprogComponent } from './groupprog.component';

describe('GroupprogComponent', () => {
  let component: GroupprogComponent;
  let fixture: ComponentFixture<GroupprogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupprogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupprogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
