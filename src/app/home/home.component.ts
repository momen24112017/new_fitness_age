import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import 'bootstrap';
import {ServicesService} from '../services.service';
import { ToastrService } from 'ngx-toastr';
import { Router }         from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public loginSpinner = false;
  public registerSpinner = false;
  public objRegister = {email:'',firstName:'',lastName:'',password:'',birthdate:'',gender:'',mobile:'',strCountry:"0",country:"0",country_code:"0"};
  public objLogin = {email:'',password:''}; 
  public arrCountry:any = [];
  public objReset = {email:'',Password:''};

  public resetSpinner = false;
  public obj:any;
  constructor(private _servicesService:ServicesService,private toastr: ToastrService, private router: Router) { }

  
  ngOnInit() {
    this.listCountry();
  }

  public openResetD = function(){
    $("#LogD").modal()
  }
  public resetPassword= function(){
        
       
    this.resetSpinner = true;
    
    return this._servicesService.Reset(this.objReset).subscribe((data:Response)=>{
        
            
            this.resetSpinner = false;
            
            this.toastr.success('success!', 'check you mailfor varifcation!');
            $("#LogD").modal('hide');
        
    })
  }
  login(){
    this.loginSpinner = true;
    return this._servicesService.login(this.objLogin).subscribe((data:Response)=>{
      this.loginSpinner = false;
      this.obj = data;
      localStorage.setItem("user_id",this.obj._id)
      this.toastr.success('Login!', 'Logged Successfully!');
      this.router.navigate(['../videos']);
    })

  }
  listCountry(){
    return this._servicesService.listCountry().subscribe((data:Response)=>{
      this.arrCountry = data;
    })
  }
  register(){
    this.registerSpinner = true;
    let x = this.objRegister.strCountry.split("-");
        this.objRegister.country_code = x[0];
        this.objRegister.country = x[1];
    return this._servicesService.register(this.objRegister).subscribe(
      (data:Response)=>{
        if(data){
          this.registerSpinner = false;
          this.toastr.success('register!', 'registered Successfully!');
        }
      }
    )
  }

}
