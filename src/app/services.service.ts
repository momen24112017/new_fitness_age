import { Injectable } from '@angular/core';
import {HttpClient ,HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class ServicesService {
 
  public arrTodayGroup = new BehaviorSubject([]);
  public arrRecentWeekGroup = new BehaviorSubject([]);

  public arrTodayFitness = new BehaviorSubject([]);
  public arrRecentWeekFitness = new BehaviorSubject([]);

  
  public arrGroupExercisePrograms = new BehaviorSubject([]);
  public arrFitnessPrograms = new BehaviorSubject([]);

  public arrBlog = new BehaviorSubject([]);

  public arrHealthVideo = new BehaviorSubject([]);

  public arrVideoLibaryGroupExerciseProgram = new BehaviorSubject([]);
  public arrVideoLibaryFitnessProgram = new BehaviorSubject([]);

  public arrVideoLibaryFitness = new BehaviorSubject([]);
  public arrVideoLibaryGroupExercise = new BehaviorSubject([]);
  
  public programNameF = new BehaviorSubject([]);
  public programNameG = new BehaviorSubject([]);
  
  public test:string;
  private _urlRegister: string = "https://newagefitness.ae/api/register";
  private _urlLogin: string ="https://newagefitness.ae/api/login";
  private _urlReset: string ="https://newagefitness.ae/api/requestresetpassword";
  private _urlVerfiy: string ="https://newagefitness.ae/api/verify"
  private _urlListCountry: string ="https://newagefitness.ae/api/getCountryList";
  private _urlTodaylasses: string ="https://newagefitness.ae/api/exercises/today";
  private _urlBlogs: string = "https://newagefitness.ae/api/blogs";
  private _urlVideoLibary:string ="https://newagefitness.ae/api/videos/category/library";
  private _urlHealthVideo:string ="https://newagefitness.ae/api/videos/category/healthy";
  private _urlListProgram:string = "https://newagefitness.ae/api/programs/exercises";
  public _urlFeedback:string = "https://newagefitness.ae/api/feedback";
  constructor(private activRoute : ActivatedRoute , private router : Router,private http: HttpClient) { }

 

  listCountry(){
    return this.http.get(this._urlListCountry);
  }

  register(objRegister){
    return this.http.post(this._urlRegister,JSON.stringify(objRegister),
    {headers: new HttpHeaders({
      'Content-Type':'application/json',
    })}
    ).map(data=>data)
  }
  login(objLogin){
    return this.http.post(this._urlLogin,JSON.stringify(objLogin),{headers: new HttpHeaders({
      'Content-Type':'application/json',
    })}
    ).map(data=>data)
  }
  reset(objReset){
    return this.http.post(this._urlReset,JSON.stringify(objReset),{headers: new HttpHeaders({
      'Content-Type':'application/json',
    })}
    ).map(data=>data)
  }
  verify(objVerify){
    return this.http.post(this._urlVerfiy,JSON.stringify(objVerify),{headers: new HttpHeaders({
      'Content-Type':'application/json',
    })}
    ).map(data=>data)
  }

  listTodayClasses(){
    return this.http.get(this._urlTodaylasses);
    // this.arrRecentWeekGroup = this.http.get(this._urlTodaylasses);
  }

  listVideoLibary(){
    return this.http.get(this._urlVideoLibary);
  }
  listBlogs(){
    return this.http.get(this._urlBlogs);
  }
  listHealthVideo(){
    return this.http.get(this._urlHealthVideo);
  }
  listProgram(){
    return this.http.get(this._urlListProgram);
  }
  addFeedback(objFeedback){
    return this.http.post(this._urlFeedback,JSON.stringify(objFeedback),
    {headers: new HttpHeaders({
      'Content-Type':'application/json',
    })}
    ).map(data=>data)
  }
  video (obj){

    localStorage.setItem("embVideo", obj.emb);
    localStorage.setItem("exercise_id", obj._id == undefined?obj.videoID:obj._id)
    
    localStorage.setItem('ClassImg',obj.image);
    
    window.location.href = "/class?link="+obj.image;
    // this.router.navigate(['../class?link='+obj.image]);
     
  }

}
