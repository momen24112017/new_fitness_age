import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandbookComponent } from './landbook.component';

describe('LandbookComponent', () => {
  let component: LandbookComponent;
  let fixture: ComponentFixture<LandbookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandbookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
