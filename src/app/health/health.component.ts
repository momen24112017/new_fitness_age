import { Component, OnInit } from '@angular/core';
import {ServicesService} from '../services.service';
@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.css']
})
export class HealthComponent implements OnInit {

  constructor(private _servicesService:ServicesService) { }
  public arrHealthVideo = [];
  
  ngOnInit() {
    this._servicesService.arrHealthVideo.subscribe((data) => {
      console.log(data);
      this.arrHealthVideo = data;
      
    });

  }
  public video = function(obj){
    return this._servicesService.video(obj).subscribe((data:Response)=>{

    })
  }

}
