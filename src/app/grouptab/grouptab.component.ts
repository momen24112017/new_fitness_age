import { Component, OnInit } from '@angular/core';
import {ServicesService} from '../services.service';
import * as $ from 'jquery';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
@Component({
  selector: 'app-grouptab',
  templateUrl: './grouptab.component.html',
  styleUrls: ['./grouptab.component.css']
})
export class GrouptabComponent implements OnInit {
  
  public arrRecentWeekGroup:any =[];
  public arrTodayGroup:any = [];
  public arrGroupExercisePrograms:any = [];
  public MDetails:any;
  public className:any;
  public Schedule:any;
  public srcVidoDialog: SafeUrl;
  constructor(private _servicesService:ServicesService,private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this._servicesService.arrRecentWeekGroup.subscribe((data) => {
      // console.log(data);
      this.arrRecentWeekGroup = data;

    });
    this._servicesService.arrTodayGroup.subscribe((data) => {
      // console.log(data);
      this.arrTodayGroup = data;
      
    });
    this._servicesService.arrGroupExercisePrograms.subscribe((data) => {
      console.log(data);
      this.arrGroupExercisePrograms = data;
      
    });
    
  }
  public video = function(obj){
    return this._servicesService.video(obj).subscribe((data:Response)=>{

    })
  }
  public getId = function(url) {
    url = url.trim();
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
      ? match[2]
      : null;
  }
  public openModalProgramDetails = function(obj){
        
    this.MDetails = obj.program.desc;
    this.className = obj.program.name;
    this.Schedule = obj.data
    if(obj.program.video != ''){
        var videoId = this.getId(obj.program.video);
        var video = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
        this.srcVidoDialog = video;
        // var video = obj.program.video;
    // video.src = video
    // $scope.VName = obj.program.video
    // video.play();
    }
    if(this.Schedule.length >0){
      $("#myDModal").modal()
    }
    
  }
  
  
}
