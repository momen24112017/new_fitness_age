import { Component, OnInit } from '@angular/core';
import {ServicesService} from '../services.service';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';


@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {
  public src:SafeUrl;
  public classImg:any = ''
  public objUser={'review':'','user':'','_id':localStorage.getItem('exercise_id'),'rating':0}
  public rate:any = 0;
  public rateSpinning:boolean = false;
  public currentRate = 0;
  public stars = 0;
  public user:any;
  constructor(private _servicesService:ServicesService,private toastr: ToastrService,private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.src = localStorage.getItem("embVideo")
    this.user = localStorage.getItem('user_id')
    this.classImg = localStorage.getItem('ClassImg');
    this.rateSpinning = false;

  }
  public addFeedback = function(){
    this.rateSpinning = true;
    this.objUser.rating = this.currentRate;
    this.objUser.user = this.user;
    this.objUser._id=localStorage.getItem('exercise_id')
    return this._servicesService.addFeedback(this.objUser).subscribe((data:Response)=>{
      this.rateSpinning = false;
      this.objUser={'review':'','user':localStorage.getItem('user'),'_id':localStorage.getItem('exercise_id'),'rating':0}
      this.toastr.success('success!', 'thank you for rating us!');
    })
  }
 
  public openRateDialog = function(){
    $('#post-review-box').modal();
  }

  public setStars = function(value){
    this.stars = value;
  };
  public litStars = function(value){
      if(this.stars>=value){
        return { "background-color" : "yellow" };
      }
      else {
        return { "background-color" : "white" };
      }
  };

}
