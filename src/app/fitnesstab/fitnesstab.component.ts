import { Component, OnInit ,EventEmitter,Output} from '@angular/core';
import {ServicesService} from '../services.service';
import * as $ from 'jquery';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
@Component({
  selector: 'app-fitnesstab',
  templateUrl: './fitnesstab.component.html',
  styleUrls: ['./fitnesstab.component.css']
})
export class FitnesstabComponent implements OnInit {
  public arrTodayFitness:any = [];
  public arrRecentWeekFitness:any = []
  public arrFitnessPrograms:any = [];
  public MDetails:any;
  public className:any;
  public Schedule:any;
  public srcVidoDialog: SafeUrl;
  constructor(private _servicesService:ServicesService,private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this._servicesService.arrRecentWeekFitness.subscribe((data) => {
      // console.log(data);
      this.arrRecentWeekFitness = data;

    });
    this._servicesService.arrTodayFitness.subscribe((data) => {
      // console.log(data);
      this.arrTodayFitness = data;
      
    });
    this._servicesService.arrFitnessPrograms.subscribe((data) => {
      // console.log(data);
      this.arrFitnessPrograms = data;
      
    });
    
  }
  public video = function(obj){
    return this._servicesService.video(obj).subscribe((data:Response)=>{

    })
  }
  public getId = function(url) {
    url = url.trim();
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
      ? match[2]
      : null;
  }
  
  public openModalProgramDetails = function(obj){
        
    this.MDetails = obj.program.desc;
    this.className = obj.program.name;
    this.Schedule = obj.data
    if(obj.program.video != ''){
        var videoId = this.getId(obj.program.video);
        var video = "https://www.youtube.com/embed/"+ videoId +"?enablejsapi=1&autohide=1&rel=0";
        this.srcVidoDialog = video;
  
    }
    if(this.Schedule.length >0){
      $("#myDModalF").modal()
    }
    
  }

}

