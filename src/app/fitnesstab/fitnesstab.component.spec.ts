import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnesstabComponent } from './fitnesstab.component';

describe('FitnesstabComponent', () => {
  let component: FitnesstabComponent;
  let fixture: ComponentFixture<FitnesstabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitnesstabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitnesstabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
