import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogtabComponent } from './blogtab.component';

describe('BlogtabComponent', () => {
  let component: BlogtabComponent;
  let fixture: ComponentFixture<BlogtabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogtabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogtabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
