import { Component, OnInit } from '@angular/core';
import {ServicesService} from '../services.service';
import * as $ from 'jquery';
@Component({
  selector: 'app-blogtab',
  templateUrl: './blogtab.component.html',
  styleUrls: ['./blogtab.component.css']
})
export class BlogtabComponent implements OnInit {

  constructor(private _servicesService:ServicesService) { }
  public arrBlog = [];
  public BlogDetails:any;
  ngOnInit() {
    this._servicesService.arrBlog.subscribe((data) => {
      
      this.arrBlog = data;
      
  });
  // this.ListBlog()
  }

  public openModalBlogDetails = function(obj){
    this.BlogDetails = obj;
    $("#myBlogModal").modal();
  }

}
