import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ClassComponent} from './class/class.component';
import {StayComponent} from './stay/stay.component';
import {VideosComponent} from './videos/videos.component';
import {GrouptabComponent} from './grouptab/grouptab.component';
import {FitnesstabComponent} from './fitnesstab/fitnesstab.component';
import {LibarytabComponent} from './libarytab/libarytab.component';
import {BlogtabComponent} from './blogtab/blogtab.component';
import {HealthComponent} from './health/health.component';
import {IntroComponent} from './intro/intro.component';
import {LandbookComponent} from './landbook/landbook.component';
import { NationalityComponent } from './nationality/nationality.component';

const routes: Routes = [
  {path:'',component:IntroComponent},
  {path:'class',component:ClassComponent},
  {path:'stay',component:StayComponent},
  {path:'videos',component:VideosComponent},
  {path:'grouptab',component:GrouptabComponent},
  {path:'fitnesstab',component:FitnesstabComponent},
  {path:'libarytab',component:LibarytabComponent},
  {path:'blogtab',component:BlogtabComponent},
  {path:'health',component:HealthComponent},
  {path:'home',component:HomeComponent},
  {path:'landbook',component:LandbookComponent},
 { path: 'nationality',
  component: NationalityComponent,
  data: { title: 'national Select ' }
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingcomponet =[HomeComponent,StayComponent,ClassComponent,VideosComponent,GrouptabComponent,FitnesstabComponent,LibarytabComponent,BlogtabComponent,HealthComponent,IntroComponent,LandbookComponent]